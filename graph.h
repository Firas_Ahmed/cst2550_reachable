#ifndef _GRAPH_H_
#define _GRAPH_H_

#include <vector>

class Digraph {

private:
  const unsigned VERTEX_QTY;
  unsigned edge_qty;
  std::vector<std::vector<unsigned>> adjacency_list;

public:
  
  Digraph(unsigned vertices) :
    VERTEX_QTY(vertices),
    adjacency_list(vertices, std::vector<unsigned>()) {}

  /* V: get the number of vertices */
  unsigned V() const;
  
  /* E: get the number of edges */
  unsigned E() const;
  
  /* add_edge: add an edge from vertex v1, to vertex v2 */
  void add_edge(unsigned v1,unsigned v2);

  /* adjacent: get a std::vector of all vertices adjacent 
     to the given vertex */
  std::vector<unsigned> adjacent(unsigned vertex);

  /* reverse: get a Digraph with the same vertices 
     as this one, but the direction of all edges 
     are reversed */
  Digraph reverse();
};

class DirectedDFS {
private:
  std::vector<bool> marked;

  /* dfs: perform depth-first search on the given digraph starting from
     vertex start and marking all visited vertices */
  void dfs(Digraph graph, unsigned start);
  
public:
  /* constructor: initialise members and perform DFS on the given
     digraph */
  DirectedDFS(Digraph graph, unsigned start);

  /* is_marked: check if the given vertex is marked (has been visited
     during DFS) */
  bool is_marked(unsigned vertex);
};

#endif /* _GRAPH_H_ */
